module main
import os

struct Empire {
     name string
}

struct Game {
     turns int
}

fn main() {
     println('Star Empire 4x Game')
     println('What would you like to name your empire?')
     empire := Empire{ os.get_line().trim_space() }
     println('How many turns would you like to play?')
     turns := os.get_line().trim_space()
     game := Game{ turns.int() }
     println('You are going to play the $empire.name for $game.turns turns!')
}
